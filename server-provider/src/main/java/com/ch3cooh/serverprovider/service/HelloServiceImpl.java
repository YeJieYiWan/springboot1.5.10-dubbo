package com.ch3cooh.serverprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.ch3cooh.serverapi.service.HelloService;

@Service(
        version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}"
)
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String name) {
        return "welcomt to dubbo! " + name + " , springboot 1.5.10.RELEASE";
    }
}
