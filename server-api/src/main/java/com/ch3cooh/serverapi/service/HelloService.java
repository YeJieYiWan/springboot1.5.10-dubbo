package com.ch3cooh.serverapi.service;

public interface HelloService {

     String hello(String name);

}
