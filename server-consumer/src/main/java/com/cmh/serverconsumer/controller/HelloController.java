package com.cmh.serverconsumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.ch3cooh.serverapi.service.HelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Reference(version = "1.0.0",
            application = "${dubbo.application.id}",
            url = "dubbo://localhost:12345")
    private HelloService helloService;

    @RequestMapping("/say")
    public String sayHello(@RequestParam String name){
        return helloService.hello(name);
    }

}
